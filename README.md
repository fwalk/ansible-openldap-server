OpenLDAP Server
=========

An Ansibe Role that installs an OpenLDAP server on a Debian flavored Linux.

Requirements
------------

None.

Role Variables
--------------

| Variable | Description | Example |
|----------|-------------|---------|
| openldap_server_app_path | Install directory| /etc/ldap/ |
| openldap_server_organization | LDAP Org| example |
| openldap_server_domain_name | LDAP Domain | example.org |
| openldap_server_rootpw | Root password | password |
| templatedir | Directory for template processing| /root/ldap/ |
| openldap_user_read_password | Read user password | secret |
| openldap_certkey_name | LDAP-Secure key| privkey.pem |
| openldap_cert_name | LDAP-Secure certificate | fullchain.pem |

Dependencies
------------

None.

Molecule tests make use of

- geerlingguy.ntp
- weareinteractive.ufw
- hadrienpatte.self_signed_certificate

Example Playbook
----------------

```plain
---
- hosts: all
  become: yes
    - role: ansible-openldap-server
      vars:
        openldap_server_app_path: /etc/ldap/
        openldap_server_organization: example
        openldap_server_domain_name: example.org
        openldap_server_rootpw: password
        templatedir: /root/ldap/
        openldap_user_read_password: secret
        openldap_certkey_name: privkey.pem
        openldap_cert_name: fullchain.pem
```

License
-------

BSD

Author Information
------------------

fwalk___gitlab
