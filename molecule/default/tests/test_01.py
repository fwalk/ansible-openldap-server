import os
# import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_sladp_is_listening_at_389(host):
    assert host.socket("tcp://0.0.0.0:389").is_listening


# def test_sladp_is_listening_at_636(host):
#     assert host.socket("tcp://0.0.0.0:636").is_listening
